.. Tuto Securité documentation on Fri Mar 22 2019.


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/opsindev/secops/rss.xml>`_

.. _security_tuto:
.. _tuto_secops:

=====================
**Opsindev secops**
=====================

- https://fr.wikipedia.org/wiki/Portail:S%C3%A9curit%C3%A9_informatique
- https://infosec.mozilla.org/guidelines/web_security
- https://docs.djangoproject.com/en/dev/topics/security/
- https://observatory.mozilla.org/analyze.html?host=appcanary.com
- https://internethealthreport.org/v01/fr/privacy-and-security/

.. toctree::
   :maxdepth: 6

   news/news

.. toctree::
   :maxdepth: 3


   authentication/authentication
   people/people
   organisms/organisms
   frameworks/frameworks
   web_security_exploits/web_security_exploits
   openssh/openssh
   faq/faq
