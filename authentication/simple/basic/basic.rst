.. index::
   pair: Basic ; Authentication

.. _basic_authentication:

======================
Basic Authentication
======================

.. seealso::

   - https://en.wikipedia.org/wiki/Basic_access_authentication
   - https://tools.ietf.org/html/rfc7617


.. contents::
   :depth: 3


Wikipedia Definition
=====================

HTTP Basic authentication (BA) implementation is the simplest technique for
enforcing access controls to web resources because it does not require cookies,
session identifiers, or login pages; rather, HTTP Basic authentication uses
standard fields in the HTTP header, removing the need for handshakes.


The BA mechanism provides no confidentiality protection for the transmitted
credentials.
They are merely encoded with Base64 in transit, but not encrypted or hashed in
any way.

**Therefore, Basic Authentication is typically used in conjunction with HTTPS
to provide confidentiality.**
