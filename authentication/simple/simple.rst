.. index::
   pair: Simple ; Authentication

.. _simple_authentication:

======================
Simple Authentication
======================

.. seealso::

   - https://fr.wikipedia.org/wiki/Authentification_simple

.. toctree::
   :maxdepth: 3


   basic/basic
