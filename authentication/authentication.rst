.. index::
   pair: Security ; Authorization
   pair: Security ; Authentication

.. _authentication:

==================================
Authorization and Authentication
==================================

- https://en.wikipedia.org/wiki/Authentication

.. toctree::
   :maxdepth: 5


   simple/simple
   tutorials/tutorials
