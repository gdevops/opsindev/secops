
.. index::
   pair: scp ; outdated

.. _openssh_def:

=====================
openSSH definition
=====================

OpenSSH is the premier connectivity tool for remote login with the SSH protocol.

It encrypts all traffic to eliminate eavesdropping, connection hijacking, and
other attacks.

In addition, OpenSSH provides a large suite of secure tunneling capabilities,
several authentication methods, and sophisticated configuration options.


The OpenSSH suite consists of the following tools:

- Remote operations are done using ssh, scp, and sftp.
- Key management with ssh-add, ssh-keysign, ssh-keyscan, and ssh-keygen.
- The service side consists of sshd, sftp-server, and ssh-agent.


scp is outdated
-----------------

.. warning:: The scp protocol is outdated, inflexible and not readily fixed.
   We recommend the use of more modern protocols like sftp and rsync for file
   transfer instead."
