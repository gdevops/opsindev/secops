
.. _openssh:

=====================
openSSH
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/OpenSSH
   - https://www.openssh.com/
   - https://anongit.mindrot.org/openssh
   - https://github.com/openssh/openssh-portable (clone)

.. figure:: logo_openssh.png
   :align: center

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
