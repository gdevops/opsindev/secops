.. index::
   pair: clair ; Frameworks
   pair: Vulnerability ; Containers
   ! clair

.. _clair:

=====================================================
clair (Vulnerability Static Analysis for Containers)
=====================================================

.. seealso::

   - https://github.com/coreos/clair


.. figure:: logo_clair.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
