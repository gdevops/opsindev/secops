
.. _clair_def:

=====================================================
clair definition
=====================================================

.. seealso::

   - https://github.com/coreos/clair/blob/master/README.md

.. contents::
   :depth: 3


Clair
=====

|Build Status| |Docker Repository on Quay| |Go Report Card| |GoDoc| |IRC
Channel|

**Note**: The ``master`` branch may be in an *unstable or even broken
state* during development. Please use
`releases <https://github.com/coreos/clair/releases>`__ instead of the
``master`` branch in order to get stable binaries.

.. figure:: https://cloud.githubusercontent.com/assets/343539/21630811/c5081e5c-d202-11e6-92eb-919d5999c77a.png
   :alt: Clair Logo

   Clair Logo

Clair is an open source project for the `static
analysis <https://en.wikipedia.org/wiki/Static_program_analysis>`__ of
vulnerabilities in application containers (currently including
`appc <https://github.com/appc/spec>`__ and
`docker <https://github.com/docker/docker/blob/master/image/spec/v1.2.md>`__).

1. In regular intervals, Clair ingests vulnerability metadata from a
   configured set of sources and stores it in the database.
2. Clients use the Clair API to index their container images; this
   creates a list of *features* present in the image and stores them in
   the database.
3. Clients use the Clair API to query the database for vulnerabilities
   of a particular image; correlating vulnerabilities and features is
   done for each request, avoiding the need to rescan images.
4. When updates to vulnerability metadata occur, a notification can be
   sent to alert systems that a change has occured.

Our goal is to enable a more transparent view of the security of
container-based infrastructure.

Thus, the project was named ``Clair`` after the French term which translates
to *clear*, *bright*, *transparent*.

Getting Started
---------------

-  Learn `the terminology </Documentation/terminology.md>`__ and about
   the `drivers and data
   sources </Documentation/drivers-and-data-sources.md>`__ that power
   Clair
-  Watch `presentations </Documentation/presentations.md>`__ on the
   high-level goals and design of Clair
-  Follow instructions to get Clair `up and
   running </Documentation/running-clair.md>`__
-  Explore `the
   API <https://app.swaggerhub.com/apis/coreos/clair/3.0>`__ on
   SwaggerHub
-  Discover third party
   `integrations </Documentation/integrations.md>`__ that help integrate
   Clair with your infrastructure
-  Read the rest of the documentation on the `CoreOS
   website <https://coreos.com/clair/docs/latest/>`__ or in the
   `Documentation directory </Documentation>`__

Contact
-------

-  IRC: #\ `clair <irc://irc.freenode.org:6667/#clair>`__ on
   freenode.org
-  Bugs: `issues <https://github.com/coreos/clair/issues>`__

Contributing
------------

See `CONTRIBUTING <.github/CONTRIBUTING.md>`__ for details on submitting
patches and the contribution workflow.

License
-------

Clair is under the Apache 2.0 license. See the `LICENSE <LICENSE>`__
file for details.

.. |Build Status| image:: https://api.travis-ci.org/coreos/clair.svg?branch=master
   :target: https://travis-ci.org/coreos/clair
.. |Docker Repository on Quay| image:: https://quay.io/repository/coreos/clair/status
   :target: https://quay.io/repository/coreos/clair
.. |Go Report Card| image:: https://goreportcard.com/badge/coreos/clair
   :target: https://goreportcard.com/report/coreos/clair
.. |GoDoc| image:: https://godoc.org/github.com/coreos/clair?status.svg
   :target: https://godoc.org/github.com/coreos/clair
.. |IRC Channel| image:: https://img.shields.io/badge/freenode-%23clair-blue.svg
   :target: http://webchat.freenode.net/?channels=clair
