
.. index::
   pair: clair-local-scan; docker
   ! clair-local-scan

.. _clair_local_scan:

=====================================================
clair-local-scan (Run CoreOs Clair standalone )
=====================================================

.. seealso::

   - https://github.com/arminc/clair-local-scan
   - :ref:`clair`


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
