
.. index::
   pair: clair-local-scan; Versions

.. _clair_local_scan_versions:

=====================================================
clair-local-scan versions
=====================================================


.. toctree::
   :maxdepth: 3

   2.0.8/2.0.8
