
.. index::
   pair: clair-scanner; docker
   ! clair-scanner

.. _clair_scanner:

=====================================================
clair-scanner (Docker containers vulnerability scan)
=====================================================

.. seealso::

   - https://github.com/arminc/clair-scanner


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
