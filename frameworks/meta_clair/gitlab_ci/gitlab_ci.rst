.. index::
   pair: clair ; gitlab-ci

.. _clair_gitlab_ci:

=====================================================
clair-{local-scan,scanner} gitlab-ci integration
=====================================================

.. seealso::

   - :ref:`container_scanning_gitlab_ci`
