.. index::
   ! meta clair

.. _meta_clair:

======================
meta clair
======================

.. figure:: clair/logo_clair.png
   :align: center


.. toctree::
   :maxdepth: 5

   clair/clair
   clair_local_scan/clair_local_scan
   clair_scanner/clair_scanner
   gitlab_ci/gitlab_ci
