.. index::
   pair: bandit ; Frameworks

.. _bandit:

======================
bandit
======================

.. seealso::

   - https://github.com/PyCQA/bandit
