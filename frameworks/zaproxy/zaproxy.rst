.. index::
   pair: zaproxy ; Frameworks

.. _zaproxy:

====================================
zaproxy (OWASP Zed Attack Proxy)
====================================

.. seealso::

   - https://www.zaproxy.org/
   - https://en.wikipedia.org/wiki/Dynamic_Application_Security_Testing
   - https://github.com/zaproxy
   - https://github.com/zaproxy/zaproxy/wiki/FAQtoplevel
   - https://github.com/zaproxy/zaproxy
   - https://github.com/zaproxy/zaproxy/wiki
   - https://github.com/zaproxy/zaproxy/issues
   - https://www.openhub.net/p/zaproxy
   - https://groups.google.com/forum/#!forum/zaproxy-develop


.. figure::  zap_logo_32x32.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
   gitlab/gitlab
   python/python
