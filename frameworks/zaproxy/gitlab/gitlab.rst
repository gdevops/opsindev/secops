.. index::
   pair: zaproxy ; gitlab
   pair: DAST ; Dynamic Application Security Testing

.. _zaproxy_gitlab:

===============================================================
zaproxy && gitlab Dynamic Application Security Testing (DAST)
===============================================================

.. seealso::

   - https://docs.gitlab.com/ee/ci/examples/dast.html


.. contents::
   :depth: 3


https://github.com/zaproxy/zaproxy/wiki/ApiPython
==================================================

.. seealso::

   - https://github.com/zaproxy/zaproxy/wiki/ApiPython
