.. index::
   pair: zaproxy ; Versions

.. _zaproxy_versions:

====================================
zaproxy versions
====================================

.. seealso::

   - https://github.com/zaproxy/zaproxy/releases


.. toctree::
   :maxdepth: 3

   2.8.0/2.8.0
   2.7.0/2.7.0
