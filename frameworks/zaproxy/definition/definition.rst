
.. _zaproxy_def:

====================================
zaproxy definition
====================================

.. contents::
   :depth: 3

`|image0| OWASP ZAP <https://www.owasp.org/index.php/ZAP>`__
============================================================

|License| |GitHub release| |Build Status| |CII Best Practices| |Coverity
Scan Build Status| |Github Releases| |Javadocs| |OWASP Flagship|
|Language grade: Java| |Twitter Follow|

The OWASP Zed Attack Proxy (ZAP) is one of the world’s most popular free
security tools and is actively maintained by hundreds of international
volunteers\ `\* <#justification>`__.


It can help you automatically find security vulnerabilities in your web
applications while you are developing and testing your applications.

Its also a great tool for experienced pentesters to use for manual security
testing.

|image11|

Please help us to make ZAP even better for you by answering the `ZAP User Questionnaire <https://docs.google.com/forms/d/1-k-vcj_sSxlil6XLxCFade-m-IQVeE2h9gduA-2ZPPA/viewform>`__!
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For general information about ZAP:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `Home page <https://www.owasp.org/index.php/ZAP>`__ - the official
   ZAP page on the OWASP wiki (includes a donate button;)
-  `Twitter <https://x.com/zaproxy>`__ - official ZAP
   announcements (low volume)
-  `Blog <https://zaproxy.blogspot.com/>`__ - official ZAP blog
-  `Monthly
   Newsletters <https://github.com/zaproxy/zaproxy/wiki/Newsletters>`__
   - ZAP news, tutorials, 3rd party tools and featured contributors
-  `Swag! <https://github.com/zaproxy/zap-swag>`__ - official ZAP swag
   that you can buy, as well as all of the original artwork released
   under the CC License

For help using ZAP:
~~~~~~~~~~~~~~~~~~~

-  `Getting Started Guide
   (pdf) <https://github.com/zaproxy/zaproxy/releases/download/2.7.0/ZAPGettingStartedGuide-2.7.pdf>`__
   - an introductory guide you can print
-  `Tutorial
   Videos <https://www.youtube.com/playlist?list=PLEBitBW-Hlsv8cEIUntAO8st2UGhmrjUB>`__
-  `Articles <https://github.com/zaproxy/zaproxy/wiki/ZAP-Articles>`__ -
   that go into ZAP features in more depth
-  `Frequently Asked
   Questions <https://github.com/zaproxy/zaproxy/wiki/FAQtoplevel>`__
-  `User Guide <https://github.com/zaproxy/zap-core-help/wiki>`__ -
   online version of the User Guide included with ZAP
-  `User Group <https://groups.google.com/group/zaproxy-users>`__ - ask
   questions about using ZAP
-  IRC: irc.mozilla.org #websectools (eg `using
   Mibbit <http://chat.mibbit.com/?server=irc.mozilla.org%3A%2B6697&channel=%23websectools>`__)
   - chat with core ZAP developers (European office hours usually best)
-  `Add-ons <https://github.com/zaproxy/zap-extensions/wiki>`__ - help
   for the optional add-ons you can install
-  `StackOverflow <https://stackoverflow.com/questions/tagged/zap>`__ -
   because some people use this for everything ;)

Information about the official ZAP Jenkins plugin:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `Wiki <https://wiki.jenkins-ci.org/display/JENKINS/zap+plugin>`__
-  `Group <https://groups.google.com/forum/#%21forum/zaproxy-jenkins>`__
-  `Issue
   tracker <https://issues.jenkins-ci.org/issues/?jql=project%20%3D%20JENKINS%20AND%20component%20%3D%20zap-plugin>`__
-  `Source code <https://github.com/jenkinsci/zap-plugin>`__

To learn more about ZAP development:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `Source Code <https://github.com/zaproxy>`__ - for all of the ZAP
   related projects
-  `Wiki <https://github.com/zaproxy/zaproxy/wiki/Introduction>`__ -
   lots of detailed info
-  `Developer Group <https://groups.google.com/group/zaproxy-develop>`__
   - ask questions about the ZAP internals
-  `Crowdin (GUI) <https://crowdin.com/project/owasp-zap>`__ - help
   translate the ZAP GUI
-  `Crowdin (User Guide) <https://crowdin.com/project/owasp-zap-help>`__
   - help translate the ZAP User Guide
-  `OpenHub <https://www.openhub.net/p/zaproxy>`__ - FOSS analytics
-  `BountySource <https://www.bountysource.com/teams/zap/issues>`__ -
   Vote on ZAP issues (you can also donate money here, but 10% taken
   out)
-  `Bug Bounty Program <https://bugcrowd.com/owaspzap>`__ - please use
   this to report any potential vulnerabilities you find in ZAP

Justification
^^^^^^^^^^^^^

Justification for the statements made in the tagline at the top;)

Popularity: \* ToolsWatch Annual Best Free/Open Source Security Tool
Survey: \* 2016
`2nd <http://www.toolswatch.org/2017/02/2016-top-security-tools-as-voted-by-toolswatch-org-readers/>`__
\* 2015
`1st <http://www.toolswatch.org/2016/02/2015-top-security-tools-as-voted-by-toolswatch-org-readers/>`__
\* 2014
`2nd <http://www.toolswatch.org/2015/01/2014-top-security-tools-as-voted-by-toolswatch-org-readers/>`__
\* 2013
`1st <http://www.toolswatch.org/2013/12/2013-top-security-tools-as-voted-by-toolswatch-org-readers/>`__

Contributors: \* `Code
Contributors <https://www.openhub.net/p/zaproxy>`__ \* `ZAP core i18n
Contributors <https://crowdin.com/project/owasp-zap>`__ \* `ZAP help
i18n Contributors <https://crowdin.com/project/owasp-zap-help>`__

.. |image0| image:: https://raw.githubusercontent.com/wiki/zaproxy/zaproxy/images/zap32x32.png
.. |License| image:: https://img.shields.io/badge/license-Apache%202-4EB1BA.svg
   :target: https://www.apache.org/licenses/LICENSE-2.0.html
.. |GitHub release| image:: https://img.shields.io/github/release/zaproxy/zaproxy.svg
   :target: https://github.com/zaproxy/zaproxy/wiki/Downloads
.. |Build Status| image:: https://travis-ci.org/zaproxy/zaproxy.svg?branch=develop
   :target: https://travis-ci.org/zaproxy/zaproxy
.. |CII Best Practices| image:: https://bestpractices.coreinfrastructure.org/projects/24/badge
   :target: https://bestpractices.coreinfrastructure.org/projects/24
.. |Coverity Scan Build Status| image:: https://scan.coverity.com/projects/5559/badge.svg
   :target: https://scan.coverity.com/projects/zaproxy-zaproxy
.. |Github Releases| image:: https://img.shields.io/github/downloads/zaproxy/zaproxy/latest/total.svg?maxAge=2592000
   :target: https://zapbot.github.io/zap-mgmt-scripts/downloads.html
.. |Javadocs| image:: https://javadoc.io/badge/org.zaproxy/zap/2.7.0.svg
   :target: https://javadoc.io/doc/org.zaproxy/zap/2.7.0
.. |OWASP Flagship| image:: https://img.shields.io/badge/owasp-flagship-brightgreen.svg
   :target: https://www.owasp.org/index.php/OWASP_Project_Inventory#tab=Flagship_Projects
.. |Language grade: Java| image:: https://img.shields.io/lgtm/grade/java/g/zaproxy/zaproxy.svg?logo=lgtm&logoWidth=18
   :target: https://lgtm.com/projects/g/zaproxy/zaproxy/context:java
.. |Twitter Follow| image:: https://img.shields.io/twitter/follow/zaproxy.svg?style=social&label=Follow&maxAge=2592000
   :target: https://x.com/zaproxy
.. |image11| image:: https://raw.githubusercontent.com/wiki/zaproxy/zaproxy/images/ZAP-Download.png
   :target: https://github.com/zaproxy/zaproxy/wiki/Downloads
