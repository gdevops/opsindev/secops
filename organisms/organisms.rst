.. index::
   pair: Security ; Organisms

.. _sec_organismes:

======================
Organisms
======================

.. seealso::

   - https://fr.wikipedia.org/wiki/Portail:S%C3%A9curit%C3%A9_informatique

.. contents::
   :depth: 3

Organismes
============

.. seealso::

   - https://x.com/CERTAFr
   - https://x.com/clusif


ANSSI
------

.. seealso::

   - https://x.com/anssi_fr
   - https://www.ssi.gouv.fr/administration/guide/guide-dhygiene-informatique/


Guide hygiène informatique 2017
++++++++++++++++++++++++++++++++

.. seealso:

   - https://www.ssi.gouv.fr/uploads/2017/01/guide_hygiene_informatique_anssi.pdf


Mozilla
--------

.. seealso::

   - https://x.com/mozwebsec
   - https://observatory.mozilla.org/
   - https://www.ssllabs.com/
   - https://wiki.mozilla.org/Security/Guidelines/Web_Security
   - https://developer.mozilla.org/en-US/docs/Web/Security
   - https://wiki.mozilla.org/Security/
   - https://wiki.mozilla.org/Security/CloudSec#Security_Checklist
   - https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/web_application_security
   - https://blog.mozilla.org/security/2017/01/20/communicating-the-dangers-of-non-secure-http/
   - https://observatory.mozilla.org/
   - https://github.com/mozilla/http-observatory/
   - https://en.wikipedia.org/wiki/Dynamic_Application_Security_Testing



Gitlab
========


.. toctree::
   :maxdepth: 3

   gitlab/gitlab
