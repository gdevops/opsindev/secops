.. index::
   pair: Security ; Gitlab

.. _gitlab_team:

======================
Gitlab Security team
======================

.. seealso::

   - https://about.gitlab.com/job-families/engineering/security-engineer/
