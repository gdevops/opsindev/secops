.. index::
   pair: Secops ; FAQ

.. _faq:

==================================
FAQ
==================================

How do I use same ssh key across multiple machines ?
======================================================

- https://stackoverflow.com/questions/62020862/how-do-i-use-same-ssh-key-across-multiple-machines

First of all, **the best practice is to have one key per user per machine**.

That's the most secure approach, because it means you can remove access from
one machine independent from the other, such as if one machine is lost or stolen.
