.. index::
   pair: gitlab ; ssh

.. _gitlab_ssh:

==================================
2024-03-13 **gitlab SSH**
==================================

- https://adamtheautomator.com/gitlab-ssh/

Introduction
===============

Repeated username and password prompts are annoying and time-wasting.

If you use password authentication with your Gitlab account, each action
requires you to send over your credentials, whether as part of the command
or through an interactive prompt.

